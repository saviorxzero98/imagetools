var
canvas = document.createElement('canvas'),
context,
tempContainer = document.createElement('textarea');

/**
 * Handle click
 *
 * @param info
 * @param tab
 */
function clickCopyImageHandler(info, tab)
{
	var tmpImage = new Image;

	tmpImage.src = info.srcUrl;

	tmpImage.onload = function ()
	{
		canvas.width = tmpImage.width;
		canvas.height = tmpImage.height;
		
		context = canvas.getContext('2d');
		context.drawImage(tmpImage, 0, 0);

		tempContainer.textContent = canvas.toDataURL();
		document.body.appendChild(tempContainer);
		tempContainer.select();

		chrome.extension.getBackgroundPage().console.log(tempContainer.textContent);
		document.execCommand('copy');
		document.body.removeChild(tempContainer);
	}
}

/**
 * Handle click
 *
 * @param info
 * @param tab
 */
function clickOpenImageHandler(info, tab)
{
	var tmpImage = new Image;

	tmpImage.src = info.srcUrl;

	tmpImage.onload = function ()
	{
		canvas.width = tmpImage.width;
		canvas.height = tmpImage.height;

		context = canvas.getContext('2d');
		context.drawImage(tmpImage, 0, 0);

		var url = canvas.toDataURL();

		chrome.tabs.create(
		{
			url: url
		}
		);
	}
}

function clickDownloadImageHandler(info, tab)
{
	var tmpImage = new Image;

	tmpImage.src = info.srcUrl;

	tmpImage.onload = function ()
	{
		canvas.width = tmpImage.width;
		canvas.height = tmpImage.height;

		context = canvas.getContext('2d');
		context.drawImage(tmpImage, 0, 0);

		var url = canvas.toDataURL();
		
		chrome.downloads.download(
		{
			url: url,
			saveAs: true,
			method: "GET"
		}, function (downloadId) { console.log(downloadId); });
	}
}

/**
 * Add item to context menu
 */
chrome.contextMenus.create(
{
	'title': '在新分頁中開啟圖片',
	'type': 'normal',
	'contexts': ['image'],
	'onclick': clickOpenImageHandler
}
);
chrome.contextMenus.create(
{
	'title': '另存圖片',
	'type': 'normal',
	'contexts': ['image'],
	'onclick': clickDownloadImageHandler
}
);
chrome.contextMenus.create(
{
	'title': '複製圖片Base64資料',
	'type': 'normal',
	'contexts': ['image'],
	'onclick': clickCopyImageHandler
}
);
